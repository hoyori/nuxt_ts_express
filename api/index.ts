import express from 'express';
import { message } from './messanger';

const app = express();

app.get('/', (_, res)=>{
    res.send('hello! this is api');
});
app.get('/user', (_, res)=>{
    res.send(message);
});

export default {
    path: '/api',
    handler: app
}
