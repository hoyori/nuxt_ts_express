export default {
  env: {},
  head: {
    title: "nuxt_ts_express",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: "Nuxt.js TypeScript project" }
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" }
    ]
  },
  loading: { color: "#3B8070" },
  css: ["~/assets/css/main.css"],
  build: {
    extend(config, ctx) {
      if (ctx.isDev) {
        config.devtool = ctx.isClient ? 'source-map' : 'inline-source-map';
      }
    }
  },
  buildModules: [
    "@nuxt/typescript-build"
  ],
  modules: [
    "@nuxtjs/axios"
  ],
  axios: {},
  serverMiddleware: [
    // API middleware
    '~/.api'
  ]
}
